import java.util.Arrays;
import java.util.Random;

public class Deck{
    Card[] deckArray = new Card[22];
    Random rand;

    Deck(){
        int i = 0;
        for(Card tempCard : Card.values()){
            deckArray[i] = tempCard;
            i++;
        }
    }

    public void shuffleDeck(){
        rand = new Random();

        for (int i=0; i<deckArray.length; i++) {
            int randomIndexToSwap = rand.nextInt(deckArray.length);
            Card temp = deckArray[randomIndexToSwap];
            deckArray[randomIndexToSwap] = deckArray[i];
            deckArray[i] = temp;
        }
    }

    public void printDeck(){
        for (int i=0; i<deckArray.length; i++) {
            System.out.println(deckArray[i]);
        }

    }

}