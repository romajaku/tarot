class Main {
    public static void main(String[] args) {
        Deck deck =  new Deck();
        System.out.println("Pre shuffle");
        deck.printDeck();
        deck.shuffleDeck();
        System.out.println("\n Post shuffle\n");
        deck.printDeck();
        System.out.println("\n Good \n");
        System.out.println("\n Better \n");
        System.out.println("\n Best \n");
        System.out.println("\n hello world \n");
    }
}