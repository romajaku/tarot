public enum Card {
    FOOL(0, "The Fool"),
    MAGICIAN(1, "The Magician"),
    PRIESTESS(2, "The High Priestess"),
    EMPRESS(3, "The Empress"),
    EMPEROR(4, "The Emperor"),
    HIEROFANT(5, "The Hierofant"),
    LOVERS(6, "The Lovers"),
    CHARIOT(7, "The Chariot"),
    STRENGTH(8, "Strenght"),
    HERMIT(9, "The Hermit"),
    WHEEL(10, "The Wheel"),
    JUSTICE(11, "Justice"),
    HANGEDMAN(12, "The Hanged Man"),
    DEATH(13, "Death"),
    TEMPERANCE(14, "Temerance"),
    DEVIL(15, "The Devil"),
    TOWER(16, "The Tower"),
    STAR(17, "The Star"),
    MOON(18, "The Moon"),
    SUN(19, "The Sun"),
    JUDGEMENT(20, "Judgement"),
    WORLD(21, "The World");

    Card(int cardId, String name){
        this.cardId = cardId;
        this.name = name;
    }

    public int getCardId(){ return cardId; }

    public String getName(){ return name; }
    public String toString(){ return name; }

    private int cardId;
    private String name;

    //private String picture;

}